[![tr](https://img.shields.io/badge/turkish-red?label=language)](README.tr.md) [![en](https://img.shields.io/badge/english-white?label=language)](README.md) 
# Born2beroot
**In this project, a Linux-based web server is installed on a virtual machine. Basic security measures are implemented for the server, and participants learn about virtual machines, perform simple configurations, and finally write a script that broadcasts basic system-related information at regular intervals. The project does not involve system control; instead, the SHA checksum of the virtual machine disk is obtained and stored in a signature.txt file. During evaluation, this value is checked for verification.**

## Score

The project passed with a score of 120% because I didn't install an additional service. [Project subject](en.subject.pdf)
