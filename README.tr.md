[![tr](https://img.shields.io/badge/turkish-red?label=language)](README.tr.md) [![en](https://img.shields.io/badge/english-white?label=language)](README.md) 
# Born2beroot
**Bu projede sanal makina üzerine linux tabanlı bir web server kurulmaktadır. Projede sunucunun temel güvenliği sağlanılır, sanal makinanın ne olduğu öğrenilir, basit konfigürasyonlar da yapılır son olarak sistem ile ilgili temel bilgileri belirli aralıkta broadcast eden bir script yazılır. Proje sistem tarafından kontrol edilmez sanal makina diskinin shasum ile hash değeri alınıp signature.txt dosyasına koyulur ardından değerlendirme sırasında bu değer kontrol edilir.**

## Not

Proje ek bir servis kurmadığım için %120 ile geçmiştir. [Projenin detaylı yönergesi](en.subject.pdf)
